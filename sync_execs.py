import shutil
import os
import stat
import yaml
import logging
import argparse
from typing import Optional

def mycopy(src: str, dest: str, *, follow_symlinks:Optional[bool]=True):
  shutil.copy2(src,dest,follow_symlinks=follow_symlinks)
  os.chmod(dest,stat.S_IWUSR|stat.S_IXUSR|stat.S_IRUSR|stat.S_IRGRP|stat.S_IXGRP|stat.S_IROTH|stat.S_IXOTH)

def file_sync(list_map: list, ign:Optional[list]=None):
  logger.debug(list_map)
  for item in list_map:
    for k,v in item.items():
      if stat.S_ISDIR(os.stat(k).st_mode) and stat.S_ISDIR(os.stat(v).st_mode):
        logger.debug(f"Copying directories on both sides: {v} to {k}")
        shutil.copytree(v,k,symlinks=True,ignore=shutil.ignore_patterns(ign),copy_function=mycopy,dirs_exist_ok=True)
      elif stat.S_ISREG(os.stat(k).st_mode) and stat.S_ISDIR(os.stat(v).st_mode):
        raise ValueError(f"Attempting to copy a directory into a file")
      elif stat.S_ISDIR(os.stat(k).st_mode) and stat.S_ISREG(os.stat(v).st_mode):
        logger.debug(f"Copying file{v} into {k}")
      else:
        logger.debug("Not dirs on both sides")

parser = argparse.ArgumentParser(description="Automerge script")
parser.add_argument("-dbg","--debug", help="print more debug information", action='store_true')
parser.add_argument("-b","--branch", 
                        help="Branch to sync.\n \
                        Default is \'mdl\'")
parser.add_argument("-i","--ignore", help="Folder name patterns to ignore")
args = parser.parse_args()
loglevel = logging.DEBUG if args.debug else logging.WARNING
logger = logging.getLogger("sync_execs")
logger.setLevel(loglevel)

ch = logging.StreamHandler()
ch.setLevel(loglevel)

formatter = logging.Formatter('[{asctime}:{levelname}:{filename}#{lineno}] {msg}',style="{")
ch.setFormatter(formatter)

logger.addHandler(ch)

YAML_FILE = 'file_maps.yml'
with open(YAML_FILE,'r') as syncf:
  sync_map = yaml.safe_load(syncf)

branch = args.branch or 'mdl'

assert "FOUND_IT" not in locals(), f"Variable \'FOUND_IT\' should not have been defined at this point!"
for map in sync_map['maps']:
  if branch in map:
    if args.debug:
      for pat in map[branch]:
        print(pat)
    #  Send a list of mappings to execute on
    file_sync(map[branch], args.ignore or None)
    FOUND_IT = True

if "FOUND_IT" not in locals():
  logger.error(f"Branch: \'{branch}\' was not present in {YAML_FILE}")
  raise ValueError(f"Branch: \'{branch}\' was not present in {YAML_FILE}")





